//Pile.cpp code for final project
//pherber3 | bfremin1 | rbaber3

#include "Pile.h"
#include <iostream>
#include <fstream>

// for saving game state - must not change!
std::string Pile::toString() const {
  std::string result;
  result.append(std::to_string(size()));
  result.append("\n");
  // add all the pile elements to the string, at most 20 per line
  for (int i=0; i < size(); ++i) {
    if (i % 20 == 0 && i != 0)
      result.append("\n");
    else if (i != 0)
      result.append(" ");
    result.append(pile[i].toString());
  }
  result.append("\n");
  return result;
}

/* Move a card from this pile to another pile 

   This function should be overriden by subcalsses.
*/
bool Pile::moveCard(Pile * dest) {
  return dest->addCard(pile.back());
}

/* Move a specific card from this Pile to the destination 

   This funciton should be overriden by the Build and Hand classes
 */
bool Pile::moveCard(int num, Pile * dest) {
  return dest->addCard(pile[num]);
}

/* Read in cards to deck
 */
void Pile::readIn(std::ifstream& is) {
  std::string temp;
  int num;
  int x;
  is >> temp;
  is >> num;
  for (int i = 0; i < num; i++) {
    is >> x;
    Card y(x);
    this->pile.push_back(y);
  }
  return;
}



