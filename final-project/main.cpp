//main.cpp for final project
//pherber3 | bfremin1 | rbaber3

#include "SkipBo.h"
#include <cctype>
#include <string>
#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::string;

int main (int argc, char ** argv) {
  if (!(argc == 5 || argc == 3)) { //Must be five input arguments
    cout << "invalid program usage: invalid number of arguments" << endl;
    return 1;
  }
  std::string shuffle = argv[1];
  if (!(shuffle == "true" || shuffle == "false")) { //Make sure shuffle string is properly formatted
    cout << "invalid program usage: invalid first argument" << endl; //Actually the second argument, lol
    return 1;
  }
  if (argc == 5) {
    string str = argv[2];
    //Check to make sure num player is an integer
    for (int i = 0; i < (int) str.length(); i++) {
      if (!isdigit(str.at(i))) { cout << "invalid program usage: num players must be 2-6" << endl; return 1;}
    }
    int num = (int) atof(argv[2]);
    if (num < 2 || num > 6) { //Number of players must be between 2 and 6
      cout << "invalid program usage: num players must be 2-6" << endl;
      return 1;
    } else {
      cout << "num players is " << num << endl;
    }
    str = argv[3];
    //Check to make sure the stock size is an integer
    for (int i = 0; i < (int) str.length(); i++) {
      if (!isdigit(str.at(i))) { cout << "invalid program usage: bad stock size" << endl; return 1;}
    }
    int max = (int) atof(argv[3]);
    //Make sure we don't deal out too many cards
    if (max*num > 162 || max*num <= 0 || max > 30 || (max > 20 && num == 6)) {
      cout << "invalid program usage: bad stock size" << endl;
      return 1;
    }
    cout << "stock size is " << max << endl;
    std::string deck = argv[4];
    //Check to see if the deck file exists.
    std::fstream temp(deck);
    if (!temp) { //deck file can't be opened
      cout << "invalid program usage: can't open deck file" << endl;
      return 1;
    }
    cout << endl; //Spacing
    //Initialize skipbo game
    SkipBo match(shuffle,num,max,deck);
    //play skipbo game
    match.game();
  }
  if (argc == 3) {
    std::string filename = argv[2];
    //Check to see if we can open the game file
    std::fstream temp(filename);
    if (!temp) { //Try to open game file
      cout << "invalid program usage: can't open input game file" << endl;
      return 1;
    }
    temp.close();
    //make skipbo game
    SkipBo match(shuffle,filename);
    //play skipbo game
    match.game();
  }
  return 0;
}
