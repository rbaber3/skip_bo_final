//Draw.cpp file of final project
//pherber3 | bfremin1 | rbaber3

#include "Draw.h"
#include "Build.h"
#include <vector>
#include <algorithm>
#include <random>

/* Any card can be added to the Draw pile wihtout restriction
 */
bool Draw::addCard(const Card c) {
  pile.push_back(c);
  return 1;
}

/* Move card from the draw pile to another pile
 */
bool Draw::moveCard(Pile * dest) {
  //Card can only be moved to hands or stockpiles
  if (pile.size() == 0) {
    this->replenish();
    if (pile.size() == 0) {
      std::cout << "draw pile empty" << std::endl;
      return 0; //If there is nothing in the draw pile after trying to replenish, then return.
    }
  }
  if (dest->pileType() == "hand" || dest->pileType() == "stock") { 
    bool legal = dest->addCard(this->pile[this->pile.size()-1]); //Try to add card
    if (legal) {
      this->pile.pop_back(); //Must removed card since its now in another pile
      return 1;
    }
  }
  return 0; //Card wasn't moved
}

/* Display draw pile to screen
 */
void Draw::display() const {
  std::cout << "Draw: [XX]/"; //User can't know what's on top
  std::cout << pile.size(); //Tell user how many cards are in pile
  return;
}

/* Move a card to this pile from a build pile
 */
bool Draw::moveCard(int sentinel, Pile * from) {
  //Sentinel number gives uniqueness to the function and assures its called intentionally
  if (sentinel != -1) {
    return 0;
  }
  // Copy the burned cards into a temporary vector
  std::vector<Card> temp = ((Build*) from)->burnPile();
  //Copy the burned cards into draw pile
  for (int i = 0; i < (int) temp.size(); i++) {
    this->burn.push_back(temp[i]);
  }
  return 1;
}


/* Replenish the draw pile
 */
bool Draw::replenish() { 
  if (this->burn.size() == 0) { return 0; } //No cards to replenish draw pile with.
  std::vector<Card> newPile;
  //Add card to general discard pile.
  //Add general discards first so that they're on the bottom of the draw pile.
  for (int i = 0; i < (int)this->burn.size(); i++) {
    newPile.push_back(burn[i]);
  }
  //Add the rest of the cards on top.
  for (int i = 0; i < (int)this->pile.size(); i++) {
    newPile.push_back(pile[i]);
  }
  burn.clear(); //Clears the gen. discard pile. 
  this->pile = newPile;
  return 1;
}


/**Shuffles the draw pile.
 */
void Draw::shuf() {
  std::default_random_engine rng = std::default_random_engine {};
  std::shuffle(std::begin(pile),std::end(pile),rng);
}

