//SkipBo.cpp file for final prject
//pherber3 | bfremin1 | rbaber3

#include "SkipBo.h"
#include <fstream>
#include <cstdlib>
#include <istream>

using std::string;
using std::cout;
using std::cin;
using std::endl;


/** This constructor is used if we want to start a new game
 * (i.e. user entered user entered 5 command-line args)
 */
SkipBo::SkipBo(string s, int num, int max, string deck) {
  //Initializes the current player to Player0.
  this->current = 0;
  //If we want to shuffle, start with a random player and set a shuffle
  //var which will later be used to shuffle the deck.
  if (s == "true") {
    this->shuffle = 1;
    srand(time(0)); //Must seed random number, idk why, lol
    int x = rand()%num;
    this->current = x;
  } else {
    this->shuffle = 0;
  }
  this->numPlayers = num;
  this->stockMax = max;

  //Makes a new draw pile.
  this->draw = new Draw();
  //Initializes an array of pointers that point to build piles.
  Build ** tempBuild = new Build*[4];
  this->build = tempBuild;
  //Builds the build piles.
  for (int i = 0; i < 4; i++) {
    build[i] = new Build();
  }
  //Pointer to array of player pointers.
  players = new Player*[num];
  //Initializes the players.
  for (int i = 0; i < num; i++) {
    players[i] = new Player(max);
  }
  //Load the input deck file.
  this->loadDeck(deck);
}


/** This constructor is called when we want to load a saved 
 * game state (i.e. when the user inputs 3 command-line args.
 */
SkipBo::SkipBo(string shu, string gameFile) {
  std::ifstream is;
  is.open(gameFile);
  string s;
  int num;
  int cur;
  is >> s; //do we shuffle or not
  is >> num; //num players
  is >> cur; //current player
  if (shu == "true") {this->shuffle = 1;}
  else {this->shuffle = 0;}
  this->numPlayers = num;
  this->stockMax = 162; // No means of conveying this from game to game
  this->current = cur;
  //Initialize an array of pointers to players.
  this->players = new Player*[num];
  for (int i = 0; i < num; i++) {
    int k = (i + cur)%num;
    this->players[k] = new Player(this->stockMax);
    this->players[k]->readIn(is);
  }
  //Make draw piles.
  this->draw = new Draw();
  //Call the readIn function for Draw to fill it out.
  this->draw->readIn(is);
  this->build = new Build*[4];
  for (int i = 0; i < 4; i++) {
    this->build[i] = new Build();
    this->build[i]->readIn(is);
  }
  std::cout << std::endl;
}


/** Deconstructor for any SkipBo game.
 */
SkipBo::~SkipBo() {
  delete this->draw;
  for (int i = 0; i < 4; i++) {
    delete this->build[i]; //Must free individual build piles before freeing the pointer to the array.
  }
  delete[] this->build;
  for (int i = 0; i < this->numPlayers; i++) {
    delete this->players[i]; //Same as build.
  }
  delete[] this->players;
}


/** Function to handle the processes of the game.
 * Used to call other helper functions that perform specific tasks
 * for the game.
 */
void SkipBo::game() {
  bool gameIsOver = 0; //Controller variable for while loop.
  string input = "";
  bool turnIsOver = 0;
  //Shuffle the deck if shuffle is true.
  if (this->shuffle) { this->shuf(); }
  this->deal(); //Deal cards to players.
  cout << " >> Player" << current << " turn next" << endl;

  //Entering the loop for the game.
  while (!gameIsOver) {
    gameIsOver = prompt(); //Prompts user for P/S/Q input.
    players[current]->draw(this->draw); //Current player fills hand.

    //Enter player turn loop.
    while (!turnIsOver && !gameIsOver) {
      if (!gameIsOver) {this->display(current);} //Display the state of game for the current player.
      cout << "(m)ove [start] [end] or (d)raw ? ";
      cin >> input;
      //Return if EOF (from piping in input).
      if (input == "") {return;}
      //If they put in an invalid input.
      if (!(input == "m" || input == "d")) {
	cout << "illegal command, try again" << endl;
      }
      //If they want to move a card.
      if (input == "m") {
	string f; string t; //Take as string so we ignore whitespace.
	cin >> f; cin >> t;
	//Should only take in 1 char.
	if ((f.length() == 1) && (t.length() == 1)) {
	  char fr = f.at(0); char t2 = t.at(0); //Convert char to string.
	  int from = fr - '0'; int to = t2 - '0'; //Converting chars into ints.
	  if (to > 9) { to += '0'; } //If to > 9 then it was already an alpha char (convert back).
	  bool legal = players[current]->move(from,to,build); //Attempt to move card.
	  //If they legally moved it to a discard pile, their turn is over.
	  if (to >= 1 && to <= 4 && legal) {turnIsOver = 1;}
	  if (!legal) {
	    cout << "illegal command, try again" << endl;
	  }
	  //Checks to see if the current player has won.
	  if (players[current]->isWinner()) {
	    cout << endl << "GAME OVER - Player" << current << " wins!";
	    gameIsOver = 1;
	    turnIsOver = 1;
	    //Are we supposed to save here? //LOL, nope
	  }
	 //Invalid [start] or [end] target.
	} else {
	  cout << "illegal command, try again" << endl;
	}
      }
      //If they want to draw.
      if (input == "d") {
	//If no cards in cur player hand, then draw. Else print error message.
	if (players[current]->handSize() == 0) {
	  players[current]->draw(this->draw);
	} else {
	  cout << "illegal command, try again" << endl;
	}
      }
      // Check if any build piles were filled during this turn.
      for (int i = 0 ; i < 4; i++) {
	if (build[i]->burnPile().size() != 0) {
	  build[i]->moveBurn(draw);
	  char ch = 'a' + i;
	  cout << "build pile " << ch << " full, set aside" << endl;
	}
      }
      //Check if draw pile is empty
      if (this->draw->size() == 0) { this->draw->replenish(); }
      cout << endl; //Spacing
    } //end of player turn

    //Only do this block if the game isn't over.
    if (!gameIsOver) {
      this->display(current); //At the end of a turn display the old player's status.
      turnIsOver = 0; //Reset to false for new player.
      current++; //Go to next player.
      if (current >= numPlayers) {current %= this->numPlayers;} //Iterate back to Player0.
      cout << endl << " >> Player" << current << " turn next" << endl;
    }
  } // end of game
}


/** This function will display the state of the game
 * from the current player's viewpoint.
 */
void SkipBo::display(int current) {
  draw->display(); //Print draw pile.
  cout << "  Build Piles:";
  //Print the build piles.
  for (int i = 0; i < 4; i++) {
    build[i]->display();
  }
  cout << " " << endl; //spacing
  cout << "Current Player" << current << ":  ";
  players[current]->display(); //Print the current player's data.
}


/** This function will load the deck from a filename input by the user.
 */
void SkipBo::loadDeck(string file) {
  std::fstream fs;
  fs.open(file);
  int x;
  //Reads in card numbers to the draw pile.
  while (fs >> x) {
    Card temp(x);
    this->draw->addCard(temp);
  }
  fs.close();
}


/** This function will deal the cards to the players.
 * It starts with the current player then goes in ascending order.
 */
int SkipBo::deal() {
  //If draw pile is not full, return false. 
  if (draw->size() != 162) {
    return 0;
  }
  int cardsDealt = this->numPlayers*this->stockMax; //Tells you how many cards to deal.
  int temp = current;
  int count = 0;
  for (int i = 0; i < cardsDealt; i++) {
    this->draw->moveCard(players[temp]->getStock()); //Moves a card from draw pile to player's stock pile.
    temp++; //Increment player number.
    //If it loops around go back to the first player.
    if (temp >= this->numPlayers) {temp = temp%numPlayers;}
    count++;
  }
  return count; 
}


/** This function will save the game to a filename input by the user.
 */
void SkipBo::save(string file) {
  std::ofstream fs;
  fs.open(file);
  //Determines string representation of shuffle. 
  std::string s = "false";
  if (this->shuffle) { s = "true"; }
  //Prints to file shuffle boolean, numPlayers, current player.
  fs << s << " " << numPlayers << " " << current << "\n";
  //Prints each player's data to file.
  for (int i = 0; i < numPlayers; i++) {
    int k = (i + current)%this->numPlayers; //Start with saving the current player's data.
    fs << "Player" << k << "\n";
    fs << "Stock ";
    //Everything below just prints out player's card data to file.
    fs << players[k]->stockString(); 
    fs << "Hand ";
    fs << players[k]->handString();
    fs << players[k]->discardString();
  }
  this->draw->replenish(); //Add the general discard cards to the BOTTOM of the draw pile.
  if (this->shuffle) { this->shuf(); } //Shuffle if shuffle is true after cards have ben added.
  fs << "Draw ";
  fs << this->draw->toString(); //Prints draw pile to file.
  char temp = 'a';
  //Prints all the builds piles to file.
  for (int i = 0; i < 4; i++) {
    fs << "Build_" << temp << " ";
    fs << this->build[i]->toString();
    temp++;
  }
  fs.close();
}


/** This function will display the P/S/Q prompt to the user.
 */
bool SkipBo::prompt() {
  string input = "abc"; //Initialize input to non-empty string.
  bool gameIsOver = 0;
  cout << "(p)lay, (s)ave, or (q)uit ? ";
  cin >> input;
  //Only accept valid input.
  while (!(input == "p" || input == "s" || input == "q")) {
    //cout << "illegal command, try again" << endl << "(p)lay, (s)ave, or (q)uit ? ";
    cout << "illegal command, try again" << endl << endl;
    cout << " >> Player" << current << " turn next" << endl;
    cout << "(p)lay, (s)ave, or (q)uit ? ";
    cin >> input;
  }
  //If user wants to save.
  if (input == "s") {
    cout << "save filename: ";
    string file;
    cin >> file;
    this->save(file);
    gameIsOver = 1; //Game has ended because user has elected to save.
  }
  //If user wants to quit.
  if (input == "q") {
    gameIsOver = 1; //Game is over because user wants to quit.
    cout << "thanks for playing" << endl;
  }
  return gameIsOver;
}
