//Stock.cpp file for final
//pherber3 | bfremin1 | rbaber3

#include "Stock.h"

/* Move card from stock pile to dest
 */
bool Stock::moveCard(Pile * dest) {
  //Stockpile cards can only be sent to dest cards
  if (dest->pileType() == "build") {
    bool legal = dest->addCard(this->pile[pile.size()-1]); //attempt to add card to build pile
    if (legal) {
      pile.pop_back(); //Remove card form this pile
      return 1;
    }
  }
  return 0; //Card not moved
}

/* Add card to stock
 */
bool Stock::addCard(const Card c) {
  //Cards can only be added until stock max is reached
  if ((int) pile.size() < (int) this->max) {
    pile.push_back(c);
    return 1;
  }
  return 0; //Stockpile full
}

/* Display stock to user

   Modified code from skeleton
 */
void Stock::display() const {
  std::cout << "Stock{0}: ";
  if (pile.size()) { pile[pile.size()-1].display(); }
  else { std::cout << "--"; }
  std::cout << "/" << pile.size() << std::endl;
  return;		      
}
