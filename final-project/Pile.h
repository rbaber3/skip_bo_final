//Pile.h for final project
//pherber3 | bfremin1 | rbaber3

#ifndef _PILE_H
#define _PILE_H

#include <vector>
#include <iostream>
#include <string>
#include "Card.h"

class Pile {
 protected:
  std::vector<Card> pile; //Cards in this pile
  std::string type;       //Tag that denotes the pile type
  
 public:
  //   Constructors   //
  Pile() : type("pile") { }
  Pile(std::string t) : type(t) { }
  
  //   Destructors   //
  virtual ~Pile() { }

  //   Non-variable Functions   //
  std::string pileType() const { return this->type; }
  int size() const { return pile.size(); }
  std::string toString() const;
  void readIn(std::ifstream& str);
  
  //   Variable Functions   //
  virtual bool addCard(const Card c) { pile.push_back(c); return 1; }
  virtual bool moveCard(Pile * dest);
  virtual bool moveCard(int num, Pile * dest);
  virtual void display() const = 0;
};

#endif
