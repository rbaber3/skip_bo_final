//Build.h file of final project
//pherber3 | bfremin1 | rbaber3

#ifndef _BUILD_H
#define _BUILD_H

#include "Card.h"
#include "Pile.h"

class Build : public Pile {
 private:
  int top;                //Returns the number that appears atop the build pile
  std::vector<Card> burn; //Pile of cards after the twelth card has been placed
  
 public:
  //   Constructors   //
 Build() : Pile("build"), top(0) { }

  //   No Destructors needed as there is no dynamically allocated memory   //

  //   Overriden Functions   //
  bool addCard(const Card c);
  void display() const;

  //   Unique Funcitons   //
  void burnStack(); //Burn the current build pile
  bool moveBurn(Pile * dest); //Moves the burn cards into the draw pile
  std::vector<Card> burnPile() {return this->burn; }  //Returns copied vector of burn pile
};

#endif
