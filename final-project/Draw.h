//Draw.h file of final project
//pherber3 | bfremin1 | rbaber3

#ifndef _DRAW_H
#define _DRAW_H

#include "Card.h"
#include "Pile.h"

class Draw : public Pile {
 private:
  std::vector<Card> burn; //Vector of cards burned from build piles
  
 public:
  //   Constructors   //
  Draw() : Pile("draw") { }

  //   No destructors needed   //

  //   Overriden Functions   //
  bool addCard(const Card c);
  bool moveCard(Pile * dest);
  bool moveCard(int sentinel, Pile * from);
  void display() const;

  //   Unique Functions   //
  bool replenish();
  void shuf();
};

#endif
