//Player.cpp file for final project
//pherber3 | bfremin1 | rbaber3

#include "Player.h"
#include <sstream>
#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::string;


/**Constructs a new player.
 */
Player::Player(int m) {
  Discard ** temp = new Discard*[4];
  this->discard = temp;
  for (int i = 0; i < 4; i++) {
    discard[i] = new Discard();
  }
  this->hand = new Hand();
  this->stock = new Stock(m);
}

/**Destroys a player.
 */
Player::~Player() {
  delete this->hand;
  delete this->stock;
  for (int i = 0; i < 4; i++) {
    delete this->discard[i];
  }
  delete[] this->discard;
}

/** Moves a card from one pile to another.
 */
bool Player::move(int from, int to, Build ** build) {
  //If the card is coming from a stock pile.
  if (from == 0) {
    //If the card is going to a build pile.
    if (to >= 97 && to <= 100) {
      return this->stock->moveCard(build[to-97]); //Move from this player's stock pile to build pile.
    }
  }
  //If the card is coming from a discard pile.
  if (from >= 1 && from <= 4) {
    //To a build pile.
    if (to >= 97 && to <= 100) {
      return this->discard[from-1]->moveCard(build[to-97]); //Move from p's discard pile to build.
    }
  }
  //If card is from hand.
  if (from >= 5 && from <= 9) {
    if (to >= 97 && to <= 100) { //Hand -> build.
      return this->hand->moveCard(from,build[to-97]);
    }
    if (to >= 1 && to <= 4) { //Hand -> discard.
      return this->hand->moveCard(from,discard[to-1]);
    }
  }
  return 0;
}


/** Displays the current player's discard, hand, and stock piles
 */
void Player::display() {
  this->stock->display();
  std::cout << "   Discards{1-4}: ";
  for (int i = 0; i < 4; i++) {
    discard[i]->display();
  }
  std::cout << std::endl;
  std::cout << "   Hand{5-9}: ";
  this->hand->display();
  std::cout << std::endl;
  return;
}


/** Return all four discard toString functions in order.
 */
std::string Player::discardString() {
  std::stringstream ss; //Use stringstream because concatenating from 4 diff piles.
  for (int i = 0; i < 4; i++) {
    ss << "Discard" << i << " ";
    ss << discard[i]->toString();
  }
  return ss.str();
}


/** Reads in cards to all the player's piles
 */
void Player::readIn(std::ifstream& is) {
  std::string playerNum;
  is >> playerNum; //Get rid of the player's name while reading in from the file.
  this->stock->readIn(is);
  this->hand->readIn(is);
  for (int i = 0; i < 4; i++) {
    this->discard[i]->readIn(is);
  }
}

