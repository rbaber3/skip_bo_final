//Player.h file for final project
//pherber3 | bfremin1 | rbaber3

#ifndef _PLAYER_H
#define _PLAYER_H

#include "Hand.h"
#include "Stock.h"
#include "Discard.h"
#include "Build.h"
#include "Draw.h"

class Player {
 private:
  Discard ** discard;
  Hand * hand;
  Stock * stock;

 public:
  //   COnstructor   //
  Player(int m);

  //   Destructor   //
  ~Player();

  //   Stock Functions   //
  Stock * getStock() {return this->stock; }
  int stockSize() {return this->stock->size(); }
  std::string stockString() { return this->stock->toString(); }

  //   Hand Function   //
  int handSize() { return this->hand->size(); }
  std::string handString() { return this->hand->toString(); }

  //   Discard Functions   //
  std::string discardString();

  //   Gameflow functions   //
  void display();
  bool move(int from, int to, Build ** build);
  int draw(Draw * draw) {return hand->refill(draw); }
  void readIn(std::ifstream& is);
  bool isWinner() {return !stock->size(); }
};

#endif
