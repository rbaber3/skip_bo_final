//Hand.cpp file for final project
//pherber3 | bfremin1 | rbaber3

#include "Hand.h"

/*Add a card to the hand
 */
bool Hand::addCard(const Card c){
  //Only add card if there are less than 5 cards
  if (pile.size() < 5) {
    pile.push_back(c);
    return 1;
  }
  return 0; //Hand already full
}

/* Move card from hand to dest
 */
bool Hand::moveCard(int num, Pile * dest) {
  if ((num-5) >= pile.size()) {
    return 0;
  }
  if (dest->pileType() == "discard") { //dest pile is a discard pile
    bool legal = dest->addCard(pile[num-5]); //Hand elements are numbered 5-9 so there is a conversion to 0-3
    if (legal) {
      this->pile.erase(pile.begin()+(num-5));
      return 1;
    }
  }
  if (dest->pileType() == "build") { //dest pile is a build pile
    bool legal = dest->addCard(pile[num-5]);
    if (legal) {
      //this->remove(num-5); //Remove the chosen element
      this->pile.erase(pile.begin()+(num-5));
      //std::cout << pile.size() << std::endl;
      return 1;
    }
  }
  return 0; //illegal move
}

/* Refill the hand from the draw pile
 */
int Hand::refill(Pile * draw) {
  if (draw->pileType() != "draw") { //Pile must be of type draw
    return -1;
  }
  int count;
  bool temp = 1;
  while (pile.size() < 5 && temp) { //keep drawing until hand is full
    temp = draw->moveCard(this); // move cards from draw pile to this pile
    count++;
  }
  return count; //Return number of cards drawn
}

/* Display Hand contents

   From skeleton code
 */
void Hand::display() const {
  int i;
  for (i = 0; i < pile.size(); i++) {
    pile[i].display();
    std::cout << "  ";
  }
  for ( ; i < 5; i++) {
    std::cout << "--  ";
  }
}

/* Remove a specific element form the hand
 */
void Hand::remove(int num) {
  int size = pile.size();
  std::vector<Card> temp = pile; //Create a temp vector to hold original hand
  pile.resize(0); //Reset the pile
  for (int i = 0; i < size; i ++) {
    if (i != num) { //Only add elements if the it is not the element the removed card
      pile.push_back(temp[i]);
    }
  }
}

