//Build.cpp file for final project
//pherber3 | bfremin1 | rbaber3

#include "Build.h"

/* Adds a card to the build pile
 */
bool Build::addCard(const Card c) {
  //If the card is a SB, allow placement regardless of top card
  if (c.getValue() == 0) {
    //Update the top card on the build pile
    top++;
    pile.push_back(c);
    //If there are now 12 cards in the buil pile, burn the pile and start anew
    if (top == 12) { this->burnStack(); top = 0; }
    return 1;
  }
  //The card value must be one larger than the current top card value
  if (c.getValue() == top + 1) {
    //Update top
    top++;
    pile.push_back(c); //Add card to pile
    if (top == 12) { this->burnStack(); top = 0; } //burn build pile if there are twelve cards;
    return 1;
  }
  // If neither of the if statement were enters, the move is invalid
  return 0;
}

/* Display build pile to screen
 */
void Build::display() const {
  std::cout << " ";
  //Print out top card of the deck.
  if (pile.size()) {
    if (pile[pile.size()-1].getValue() != 0) {
      std::cout << "[" << pile[pile.size()-1].getValue() << "]";
    } else {
      std::cout << "[SB]";
    }
  }
  //If build pile is empty.
  else { std::cout << "--"; }
  std::cout << "/" << pile.size();
}

/* Transfer cards from the build pile to the burn pile
 */
void Build::burnStack() {
  for (int i = 0 ; i < 12; i++) {
    burn.push_back(pile[i]);
  }
  //Clear the pile after transfer
  pile.clear();
}

/* Move this burn pile into the draw burn pile. This is useful for replensihing later
 */
bool Build::moveBurn(Pile * dest) {
  if (burn.size() == 0) {
    return 0; //burn must have cards to be moved
  }
  //Only tranfer burn cards to the draw pile
  if (dest->pileType() != "draw") {
    return 0;
  }
  //Actually move the burn cards to the draw pile
  dest->moveCard(-1,this);
  //Clear burn pile
  this->burn.clear();
  return 1;
}
