//Hand.h file for final project
//pherber3 | bfremin1 | rbaber3

#ifndef _HAND_H
#define _HAND_H

#include <sstream>
#include <iterator>
#include "Card.h"
#include "Pile.h"

class Hand : public Pile {
 public:
  //   Constructors   //
  Hand() : Pile("hand") {}

  //   No Destructors Needed   //

  //   Overridden Functions   //
  bool addCard(const Card c);
  bool moveCard(int num, Pile * dest);
  void display() const;

  //   Unique Functions   //
  void remove(int num);
  int refill(Pile * draw);
};

#endif
