//Discard.cpp file for final project
//pherber3 | bfremin1 | rbaber3

#include "Discard.h"

/* Any card can be added to the discard pile in any order
 */
bool Discard::addCard(const Card c) {
  pile.push_back(c);
  return 1;
}

/* Move a card from the discard pile ot another pile
 */
bool Discard::moveCard(Pile * dest) {
  if (pile.size() == 0) { return 0; } //Can't move card if nothing is there
  if (dest->pileType() == "build") {  //Can only move card to build piles
    bool legal = dest->addCard(pile.back()); //Check to see if the card can be added to the build pile
    if (legal) {
      pile.pop_back(); //The card has been added to the build pile so it must be removed here
      return 1;
    }
  }
  return 0; //Card wasn't moved
}

/* Display the card to the screen for gameplay

   This is standard from the skeleton code
 */
void Discard::display() const {
  if (pile.size()) {
    pile[pile.size()-1].display();
  }
  else { std::cout << "--"; }
  std::cout << "/" << pile.size();
  std::cout << "  ";
  return;
}
