//SkipBo.h file for final project
//pherber3 | bfremin1 | rbaber3

#ifndef _SKIPBO_H
#define _SKIPBO_H

#include "Player.h"

class SkipBo {
 private:
  Player ** players;
  Build ** build;
  Draw * draw;
  bool shuffle;
  int numPlayers;
  int stockMax;
  int current;

 public:
  //////////    CONSTRUCTORS   //////////
  SkipBo(std::string s, int num, int max, std::string deck);
  SkipBo(std::string shu, std::string gameFile);

  //////////    DESTRUCTORS   //////////
  ~SkipBo();

  //////////    FUNCTIONS     //////////
  void game();
  bool prompt();
  void save(std::string file);
  void loadDeck(std::string file);
  int deal();
  void display(int current);
  void shuf() { this->draw->shuf(); }
};

#endif
