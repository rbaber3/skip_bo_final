//Discard.h file for final project
//pherber3 | bfremin1 | rbaber3

#ifndef _DISCARD_H
#define _DISCARD_H

#include "Card.h"
#include "Pile.h"

class Discard : public Pile {
 public:
  //   Constructors   //
  Discard() : Pile("discard") {}

  //   No Destructors Needed   //

  //   Overriden Functions   //
  bool addCard(const Card c);
  void display() const;
  bool moveCard(Pile * dest);
};

#endif
