//Stock.h file for final
//pherber3 | bfremin1 | rbaber3

#ifndef _STOCK_H
#define _STOCK_H

#include "Card.h"
#include "Pile.h"

class Stock : public Pile {
 private:
  int max; //Max number of stockpile cards
  
 public:
  //   Constructors   //
  Stock(int m) : Pile("stock"), max(m) {}

  //   No Destructors Needed   //

  //   Overriden Functions   //
  bool moveCard(Pile * dest);
  bool addCard(const Card c);
  void display() const;
};

#endif

